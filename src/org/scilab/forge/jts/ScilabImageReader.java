package org.scilab.forge.jts;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.Canvas;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import javax.swing.ImageIcon;
import java.net.URL;


public class ScilabImageReader {
    
    private static final Component COMPONENT = (Component) new Canvas();

    public ScilabImageReader() {
    }

    public static byte[][] read(String image) throws Exception {
	ImageIcon icon = new ImageIcon(image);
	return read(icon);
    }

    public static byte[][] read(URL image) throws Exception {
	ImageIcon icon = new ImageIcon(image);
	return read(icon);
    }

    protected static byte[][] read(ImageIcon icon) throws Exception {
	int width = icon.getIconWidth();
	int height = icon.getIconHeight();
	BufferedImage bimg = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
	Graphics2D g2d = bimg.createGraphics();
	icon.paintIcon(COMPONENT, (Graphics) g2d, 0, 0);
	byte[] byteData = ((DataBufferByte) bimg.getRaster().getDataBuffer()).getData();
	g2d.dispose();
	
	byte[][] ret = new byte[width][height];
	for (int i = 0; i < width; i++) {
	    for (int j = 0; j < height; j++) {
		ret[i][j] = byteData[j * width + i];
	    }
	}

	return ret;
    }
}
