package org.scilab.forge.jts;

import org.scilab.modules.gui.console.ScilabConsole;
import org.scilab.modules.gui.bridge.console.SwingScilabConsole;
import org.scilab.modules.gui.console.SimpleConsole;
import org.scilab.modules.gui.console.Console;
import org.scilab.modules.console.SciConsole;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import java.net.URL;


public class ImageComponent extends JPanel {
    
    private SwingScilabConsole console = null;
    private int fontSize;
    private static JTextPane jtp;

    public ImageComponent() {
	super ();
	if (console == null) {
	    this.console = (SwingScilabConsole) (ScilabConsole.getConsole().getAsSimpleConsole());
	    this.jtp = (JTextPane) console.getConfiguration().getOutputView();
	}
    }

    public void display(URL image) {
	ImageIcon icon;
	try {
	    icon = new ImageIcon(image);
	}
	catch (Exception e) {
	    console.display ("\nProblem to load the image : " + image + "\n");
	    return;
	}

	display(icon);
    }

    public void display(String image) {
	ImageIcon icon;
	try {
	    icon = new ImageIcon(image);
	}
	catch (Exception e) {
	    console.display ("\nProblem to load the image : " + image + "\n");
	    return;
	}
	
	display(icon);
    }

    public void display(ImageIcon icon) {
	console.display ("\n ");
	
	jtp.setCaretPosition (jtp.getText().length());
	jtp.insertIcon (icon);
	console.display("\n");
    }
}
