package org.scilab.forge.jts;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import org.scilab.forge.jlatexmath.ParseException;

import org.scilab.modules.gui.console.ScilabConsole;
import org.scilab.modules.gui.bridge.console.SwingScilabConsole;
import org.scilab.modules.gui.console.SimpleConsole;
import org.scilab.modules.gui.console.Console;
import org.scilab.modules.console.SciConsole;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.Insets;

public class LaTeXComponent extends JPanel {
    
    private SwingScilabConsole console = null;
    private int fontSize;
    private static TeXIcon texi;
    private static JTextPane jtp;

    public LaTeXComponent() {
	super();
	if (console == null) {
	    this.console = (SwingScilabConsole) (ScilabConsole.getConsole().getAsSimpleConsole());
	    this.jtp = (JTextPane) console.getConfiguration().getOutputView();
	    
	    this.fontSize = console.getFont().getSize() + 4;
	    this.texi = new TeXFormula().createTeXIcon(TeXConstants.STYLE_DISPLAY, fontSize);
	    this.texi.setInsets(new Insets(1, 1, 1, 1));
	}
    }
    
    public void display(String ltx) {
	int len = ltx.length();
	if (len != 0 && ltx.charAt(0) == '$' && ltx.charAt(len - 1) == '$') {
	    ltx = ltx.substring(1, len - 1);
	    System.out.println(ltx);
	}

	try {
	    texi = new TeXFormula(ltx).createTeXIcon(TeXConstants.STYLE_DISPLAY, fontSize);
	}
	catch (ParseException e) {
	    console.display("\nMalformed LaTeX expression\n");
	    return;
	}
	
	console.display("\n ");
	
	jtp.setCaretPosition(jtp.getText().length());
	jtp.insertIcon(texi);
	console.display("\n");
    }

    public String toString() {
	return "Component to display LaTeX in Scilab's console";
    }
}
